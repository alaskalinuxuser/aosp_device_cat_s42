#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_S42G.mk

COMMON_LUNCH_CHOICES := \
    lineage_S42G-user \
    lineage_S42G-userdebug \
    lineage_S42G-eng
